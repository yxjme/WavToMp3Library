package com.north.light.liblame;

public interface WavToMp3Listener {
    void onProgress(long progress ,  long  total,String mp3FilePath) ;
}
