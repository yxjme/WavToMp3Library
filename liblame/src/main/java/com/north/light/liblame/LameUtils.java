package com.north.light.liblame;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by lzt
 * time 2021/6/10 11:43
 *
 * @author lizhengting
 * 描述：lame工具类
 */
public class LameUtils implements Serializable {
    /**
     * 是否压缩中的标识
     */
    private volatile boolean isCompress = false;

    static {
        System.loadLibrary("lame-lib");
    }


    private static class SingHolder {
        static LameUtils mInstance = new LameUtils();
    }

    public static LameUtils getInstance() {
        return SingHolder.mInstance;
    }

    /**
     * java层调用方法--获取版本号
     */
    public String getVersion() {
        return getLameVersion();
    }

    /**
     * java层调用方法--wav转换mp3
     */
    public void trainToMp3(String wav, String mp3, boolean deleteSource) throws Exception {
        if (isCompress) {
            throw new Exception("正在压缩仲");
        }
        isCompress = true;
        if (!new File(wav).exists()) {
            throw new Exception("源文件不存在");
        }
        if (!new File(mp3).exists()) {
            throw new Exception("目标文件不存在");
        }
        convertToMp3(wav, mp3);
        if (deleteSource) {
            new File(wav).delete();
        }
        isCompress = false;
    }



    /**
     * java层调用方法--wav转换mp3
     */
    public void trainToMp3(String wav, String mp3) throws Exception {
        trainToMp3(wav, mp3, false);
    }



    //native -------------------------------------------------------------------------------------

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    private native String getLameVersion();

    /**
     * wav转换成mp3的本地方法
     *
     * @param wav
     * @param mp3
     */
    private native void convertToMp3(String wav, String mp3);

    /**
     * 设置进度条的进度，提供给C语言调用
     *
     * @param progress
     */
    public void setConvertProgress(long progress, long total, String path) {
        Log.d(this.getClass().getSimpleName(), "回调的进度:" + progress + "----总进度:" + total + "----path:" + path);
        if (wavToMp3Listener!=null){
            wavToMp3Listener.onProgress(progress,total,path);
        }
    }


    WavToMp3Listener wavToMp3Listener ;
    private LameUtils setWavToMp3Listener(WavToMp3Listener wavToMp3Listener) {
        this.wavToMp3Listener = wavToMp3Listener;
        return LameUtils.this ;
    }


    /**
     * 录音文件
     *
     * @param wavFilePath
     *
     */
    public static void wavToMp3(Context context , String wavFilePath,final CallBack<String> callBack){
        File f = new File(wavFilePath);
        String name = f.getName() ;
        File targetFile = new File(getSaveDir(context) ,name+".mp3") ;
        try {
            targetFile.createNewFile() ;
            LameUtils.getInstance().setWavToMp3Listener(new WavToMp3Listener() {
                @Override
                public void onProgress(long progress, long total, String mp3FilePath) {
                    if (callBack!=null && progress == total){
                        callBack.success(mp3FilePath);
                    }
                }
            }).trainToMp3(wavFilePath, targetFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
            if (callBack!=null)
                callBack.error(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            if (callBack!=null)
                callBack.error(e.getMessage());
        }
    }



    /**
     * 本地文件保存路径
     *
     * @return
     */
    private static  String getSaveDir(Context context) {
        String fileDir;
        /**
         * 判断SD卡是否存在
         */
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            File file=new File(context.getExternalFilesDir(null) +File.separator+ "yxjRecord");
            if(!file.exists()){
                file.mkdir();
            }
            fileDir = file.getPath();
        } else {
            Log.v("sdCard", "-------------->SD card not mounted!");
            File f = context.getCacheDir();
            fileDir = f.getPath();
        }
        return fileDir;
    }
}
