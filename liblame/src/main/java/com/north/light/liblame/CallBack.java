package com.north.light.liblame;

public interface CallBack<T> {
    void success(T t);
    void error(String error);
}
